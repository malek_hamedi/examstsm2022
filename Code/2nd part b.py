import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

input1 = "../Results/DF/df_clustered.csv"
df = pd.read_csv(os.path.abspath(input1), sep = "," )

conditions = [
    (df["CLUSTER"] == 0),
    (df["CLUSTER"] == 1),
    (df["CLUSTER"] == 2),
    (df["CLUSTER"] == 3),
]
choices = [
    ("Retirement"),
    ("Business"),
    ("Onetime"),
    ("Healthy"),
]

df["CLIENT_TYPE"] = np.select(conditions, choices, default=0)

print(df.describe())

df.to_csv(os.path.abspath("../Results/DF/df_labeled.csv"))

#soup and salad = al cluster 3
#business man = 1
#retirement center= 0
#one time people= 2


