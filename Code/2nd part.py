import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import MinMaxScaler


input1 = "../Results/DF/df_first.csv"
df = pd.read_csv(os.path.abspath(input1), sep = "," )
plt.scatter(df["FIRST_COURSE"], df["SECOND_COURSE"], df["THIRD_COURSE"])
#plt.show()

#this errase the random part for the drinks.
df["FIRST_COURSE"]=df["FIRST_COURSE"]-df["D_STARTERS"]
df["SECOND_COURSE"]=df["SECOND_COURSE"]-df["D_MAINS"]
df["THIRD_COURSE"]=df["THIRD_COURSE"]-df["D_DESSERTS"]


km = KMeans(n_clusters=4)
y_predicted = km.fit_predict(df[["FIRST_COURSE","SECOND_COURSE","THIRD_COURSE"]])
df["CLUSTER"] = y_predicted

print(df.describe)
df1 = df[df["CLUSTER"] == 0]
df2 = df[df["CLUSTER"] == 1]
df3 = df[df["CLUSTER"] == 2]
df4 = df[df["CLUSTER"] == 3]
plt.scatter(df1["FIRST_COURSE"], df1["SECOND_COURSE"], df1["THIRD_COURSE"], color="green")
plt.scatter(df2["FIRST_COURSE"], df2["SECOND_COURSE"], df2["THIRD_COURSE"], color="red")
plt.scatter(df3["FIRST_COURSE"], df3["SECOND_COURSE"], df3["THIRD_COURSE"], color="blue")
plt.scatter(df4["FIRST_COURSE"], df4["SECOND_COURSE"], df4["THIRD_COURSE"], color="yellow")
plt.show()
plt.savefig(os.path.abspath(f"../Results/Graphs/2_firstcluster.png"))
fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")
ax.scatter(df1["FIRST_COURSE"], df1["SECOND_COURSE"], df1["THIRD_COURSE"], c="g", marker="o")
ax.scatter(df2["FIRST_COURSE"], df2["SECOND_COURSE"], df2["THIRD_COURSE"], c="r", marker="^")
ax.scatter(df3["FIRST_COURSE"], df3["SECOND_COURSE"], df3["THIRD_COURSE"], c="b", marker="s")
ax.scatter(df4["FIRST_COURSE"], df4["SECOND_COURSE"], df4["THIRD_COURSE"], c="y", marker="p")
ax.set_xlabel("Starters")
ax.set_ylabel("Mains")
ax.set_zlabel("Desserts")
ax.set_title("Clustering by consumption")
plt.savefig(os.path.abspath(f"../Results/Graphs/2_secondcluster3d.png"))
plt.show()

df.to_csv(os.path.abspath("../Results/DF/df_clustered.csv"))


#km = KMeans(n_clusters=4)
#y_predicted = km.fit_predict(df[["FIRST_COURSE","SECOND_COURSE","THIRD_COURSE","CLUSTER"]])
#df["CLUSTER"] = y_predicted
#print(df.describe())
#plt.scatter(df["FIRST_COURSE"], df["SECOND_COURSE"], df["THIRD_COURSE"])
#plt.show()
